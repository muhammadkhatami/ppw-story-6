function subscribe() {
  $.ajax({
    url : '',
    type : 'POST',
    data : {
      name : $('#name-form').val(),
      email : $('#email-form').val(),
			password : $('#password-form').val()
    },
    success: function(json) {
      $('#subcsribe_form').val('');
			$('#response_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Account is created</a></div>");

    },
    error: function (xhr, errmsg, err) {
      $('#response_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Error message &times;</a></div>");
    },
  });
}

function validation () {
	// body...
	$.ajax({
		url : 'emailCheck/',
		type: 'POST',
		data : {

			name : $('#name-form').val(),
			email: $('#email-form').val(),
			password: $('#pass-form').val()
		},

		success: function(response) {
			console.log(response)
			$('#validate_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>" + response.message + "</a></div>");
			if (response.message=="User valid"){
				document.getElementById('but-sub').disabled = false;
			}
			else {
				document.getElementById('but-sub').disabled = true;
			}
		},

		error: function(errmsg) {
			console.log(errmsg);
		}
	});
};

$(document).ready(function() {
	var x_timer;
	$('#name-form').keyup(function(e) {
		clearTimeout(x_timer);
		var name =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});

$(document).ready(function() {
	var x_timer;
	$('#email-form').keyup(function(e) {
		clearTimeout(x_timer);
		var email =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});

$(document).ready(function() {
	var x_timer;
	$('#pass-form').keyup(function(e) {
		clearTimeout(x_timer);
		var password =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});
