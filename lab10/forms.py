from django import forms
from .models import Subscribe

class lab10forms(forms.Form):
    name={
        'class' : 'form-control',
		'id' : 'name-form',
		'placeholder' : 'Input Your Name'
    }
    email={
        'class': 'form-control',
        'id': 'email-form',
        'placeholder': 'example@example.com',
    }
    password={
        'class': 'form-control',
        'id': 'password-form',
        'placeholder': 'isi password',
    }
    name = forms.CharField(max_length=60, widget=forms.TextInput(attrs=name), label='name')
    email = forms.EmailField(widget=forms.EmailInput(attrs=email), label='email')
    password = forms.CharField(max_length = 50, widget=forms.PasswordInput(attrs=password), label='password')
