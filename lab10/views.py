from django.shortcuts import render
from lab10.forms import lab10forms
from lab10.models import *
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError

# Create your views here.

response = {}

@csrf_exempt
def profile(request):
	all_subs = Subscribe.objects.all()
	if request.method =='POST':
		form= lab10forms(request.POST or None)
		name = request.POST['name']
		email = request.POST['email']
		password= request.POST['password']

		subs_filter = Subscribe.objects.filter(email =email)

		if (subs_filter):
			response_message: 'email sudah digunakan'
			# print(response_message)
			print('tidak bisa')

		else:
			subs = Subscribe(name =name, email = email, password=password)
			subs.save()

	else:
		form = lab10forms()

	return render(request, 'index10.html', {'subs':all_subs, 'form':form})

@csrf_exempt
def emailCheck(request):
	if request.method=='POST':
		name = request.POST['name']
		email= request.POST['email']
		password = request.POST['password']
		try:
			try:
				validate_email(email)
			except:
				return JsonResponse({'message': 'Email not valid'})

			subs_filter = Subscribe.objects.filter(email = email)

			if len(name)==0 or len(name)>30:
				return JsonResponse({'message':'Name not valid'})
			try:
				if len(subs_filter)>0:
					return JsonResponse({'message': 'Email already used'})
			except Exception as e:
				print(type(e))

			if len(password)<8 :
				return JsonResponse({'message': 'Password not valid'})
			else:
				return JsonResponse({'message' : 'User valid'})

		except Exception:
			print('Hai')
	else:
		return JsonResponse({'message': 'Something wrong'})
