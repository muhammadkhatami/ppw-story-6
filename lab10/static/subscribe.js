function subscribe() {
  $.ajax({
    url : '',
    type : 'POST',
    data : {
      name : $('#name-form').val(),
      email : $('#email-form').val(),
			password : $('#password-form').val(),
      csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
    },
    success: function(json) {
			$('#lab10forms').val('');
			$('#response_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Account is created</a></div>");
      $('#name-form').val("");
      $('#email-form').val("");
      $('#password-form').val("");
      $('#subscribe').html("");
      getSubscriber();
      alert("Subscribed");
		},
		error: function (xhr, errmsg, err) {
      console.log(err)
        $('#response_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Error message &times;</a></div>");
    },
  });
}

function validation () {
	$.ajax({
		url : 'api/emailCheck/',
		type: 'POST',
		data : {
			name : $('#name-form').val(),
			email: $('#email-form').val(),
			password: $('#password-form').val(),
      csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
		},

		success: function(response) {
			$('#validate_msg').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>" + response.message + "</a></div>");
			if (response.message == "User valid"){
				document.getElementById('id_submit').disabled = false;
			}
			else {
				document.getElementById('id_submit').disabled = true;
			}
		},

		error: function(errmsg) {
			console.log(errmsg);
		}
	});
};

$(document).ready(function() {
	var x_timer;
	$('#name-form').keyup(function(e) {
		clearTimeout(x_timer);
		var name =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});

$(document).ready(function() {
	var x_timer;
	$('#email-form').keyup(function(e) {
		clearTimeout(x_timer);
		var email =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});

$(document).ready(function() {
	var x_timer;
	$('#password-form').keyup(function(e) {
		clearTimeout(x_timer);
		var password =$(this).val();
		x_timer = setTimeout(function() {
			validation();
		}, 50);
	})
});
