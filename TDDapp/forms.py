from django import forms
from .models import Status

class statusForm(forms.Form):
    status = forms.CharField(label = "status", widget = forms.TextInput(attrs = {'size' : 100}))
