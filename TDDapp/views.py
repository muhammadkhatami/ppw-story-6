from django.shortcuts import render, redirect
from TDDapp.forms import statusForm
from TDDapp.models import *
import requests
from django.http import JsonResponse
from django.contrib.auth import logout

# Create your views here.

def home(request):
	return render(request, 'bookList.html')

def login(request):
	return render(request, 'login.html')

def log_out(request):
	logout(request)
	return redirect('bookList')

def make_status(request):
	if (request.method == "POST"):
		form = statusForm(request.POST)
		statuses = Status.objects.all()
		if (form.is_valid()):
			status = Status()
			status.status = form.cleaned_data["status"]
			status.save()
			return redirect('/')
		return redirect('/')
	else:
		form = statusForm()
		status = Status.objects.all()
		response = {'status' : status, 'form' : form}
		return render(request, 'landingPage.html', response)

def profile(request):
	return render(request, 'profile.html')

def index_json(request, search="quilting"):
	return render(request, 'bookList.html', {"search":search})

def datajson(request, search):
	jsonData = requests.get('https://www.googleapis.com/books/v1/volumes?q='+search).json()
	books = []
	for data in jsonData['items']:
		bookData = {}
		bookData['title'] = data['volumeInfo']['title']
		if 'author' in data['volumeInfo']:
			bookData['author'] = ", ".join(data['volumeInfo']['authors'])
		else:
			bookData['author'] = "Tidak ada"
		if 'publishedDate' in data['volumeInfo']:
			bookData['publishedDate'] = data['volumeInfo']['publishedDate']
		else:
			bookData['publishedDate'] = "Tidak ada"
		books.append(bookData)
	return JsonResponse({"data" : books})
