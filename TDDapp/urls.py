"""TDDproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from TDDapp import views

urlpatterns = [
    path('', views.make_status, name = 'landingPage'),
    path('profile/', views.profile, name = 'profile'),
    path('bookList/', views.index_json, name = 'bookList'),
    path('bookList/api/<str:search>/', views.datajson, name = 'booklist api'),
    path('bookList/<str:search>/', views.index_json),
    path('login/', views.login, name = 'login'),
    path('logout/', views.log_out, name = 'logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    #path('', views.home, name='home'),
]
