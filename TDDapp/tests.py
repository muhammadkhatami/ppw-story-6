from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from . import views
from .models import *
from .forms import statusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status='ngoding')

        # Retrieving all available activity
        counting_status = Status.objects.all().count()
        self.assertEqual(counting_status, 1)

    def test_form_validation_for_blank_items(self):
        form = statusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],["This field is required."]
        )

    def test_lab_6_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

class Lab6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        status = selenium.find_element_by_id('id_status')
        #status.click()
        #status.clear()
        status.send_keys("Coba-Coba")
        submit = selenium.find_element_by_id('id_submit')
        submit.send_keys(Keys.ENTER)
        assert "Coba-Coba" in self.selenium.page_source


    def test_title_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_id('id_title')
        assert "Hello" in self.selenium.page_source

    def test_link_myprofile_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_id('id_title')
        assert "my profile" in self.selenium.page_source

    def test_button_style_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        styling = selenium.find_element_by_id("id_submit")
        title = "btn" in styling.get_attribute("style")

    def test_myprofile_style_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        styling = selenium.find_element_by_id("id_myprofile")
        title = "display-4" in styling.get_attribute("style")
